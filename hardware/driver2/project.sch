EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:driver
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L IR2109 U301
U 1 1 58B2897B
P 4250 1850
F 0 "U301" H 4250 2150 60  0000 C CNN
F 1 "IR2109" H 4250 1575 60  0000 C CNN
F 2 "Local:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 4250 1850 60  0001 C CNN
F 3 "" H 4250 1850 60  0000 C CNN
	1    4250 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2000 3700 2000
Wire Wire Line
	3700 2000 3700 2100
Wire Wire Line
	3800 1700 3700 1700
Wire Wire Line
	3700 1700 3700 1600
Wire Wire Line
	4700 2000 5000 2000
Wire Wire Line
	4700 1900 4800 1900
Wire Wire Line
	4700 1800 5000 1800
Wire Wire Line
	3800 1800 3700 1800
Wire Wire Line
	3800 1900 3700 1900
$Comp
L Q_NMOS_GDS Q301
U 1 1 58B28C1C
P 6200 1600
F 0 "Q301" H 6100 1750 50  0000 R CNN
F 1 "TP50N03" H 6250 1400 50  0000 R CNN
F 2 "Local:TO-263-2Lead-DL" H 6400 1700 50  0001 C CNN
F 3 "" H 6200 1600 50  0000 C CNN
	1    6200 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 1400 6300 1300
Wire Wire Line
	6300 1800 6300 2000
Wire Wire Line
	6300 1900 6400 1900
Connection ~ 6300 1900
$Comp
L Q_NMOS_GDS Q302
U 1 1 58B28D44
P 6200 2200
F 0 "Q302" H 6100 2350 50  0000 R CNN
F 1 "TP50N03" H 6250 2000 50  0000 R CNN
F 2 "Local:TO-263-2Lead-DL" H 6400 2300 50  0001 C CNN
F 3 "" H 6200 2200 50  0000 C CNN
	1    6200 2200
	1    0    0    -1  
$EndComp
$Comp
L Q_NMOS_GDS Q303
U 1 1 58B28D82
P 6200 3300
F 0 "Q303" H 6100 3450 50  0000 R CNN
F 1 "TP50N03" H 6250 3100 50  0000 R CNN
F 2 "Local:TO-263-2Lead-DL" H 6400 3400 50  0001 C CNN
F 3 "" H 6200 3300 50  0000 C CNN
	1    6200 3300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P303
U 1 1 58B28E2D
P 7300 3450
F 0 "P303" H 7300 3600 50  0000 C CNN
F 1 "MOTOR" V 7400 3450 50  0000 C CNN
F 2 "Local:2PIN_HEAD" H 7300 3450 50  0001 C CNN
F 3 "" H 7300 3450 50  0000 C CNN
	1    7300 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	7100 3400 7000 3400
Wire Wire Line
	7100 3500 7000 3500
Text GLabel 7000 3400 0    47   Input ~ 0
VS1
Text GLabel 7000 3500 0    47   Input ~ 0
VS2
Text GLabel 6400 1900 2    47   Input ~ 0
VS1
Text GLabel 6400 3600 2    47   Input ~ 0
VS2
Wire Wire Line
	6300 3500 6300 3700
Wire Wire Line
	6300 3600 6400 3600
Connection ~ 6300 3600
$Comp
L Q_NMOS_GDS Q304
U 1 1 58B290D7
P 6200 3900
F 0 "Q304" H 6100 4050 50  0000 R CNN
F 1 "TP50N03" H 6250 3700 50  0000 R CNN
F 2 "Local:TO-263-2Lead-DL" H 6400 4000 50  0001 C CNN
F 3 "" H 6200 3900 50  0000 C CNN
	1    6200 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3100 6300 3000
Wire Wire Line
	6300 4100 6300 4200
Wire Wire Line
	6300 2400 6300 2500
$Comp
L GND #PWR01
U 1 1 58B292DB
P 2500 2800
F 0 "#PWR01" H 2500 2550 50  0001 C CNN
F 1 "GND" H 2500 2650 50  0000 C CNN
F 2 "" H 2500 2800 50  0000 C CNN
F 3 "" H 2500 2800 50  0000 C CNN
	1    2500 2800
	1    0    0    -1  
$EndComp
$Comp
L VPP #PWR02
U 1 1 58B2932A
P 6300 3000
F 0 "#PWR02" H 6300 2850 50  0001 C CNN
F 1 "VPP" H 6300 3150 50  0000 C CNN
F 2 "" H 6300 3000 50  0000 C CNN
F 3 "" H 6300 3000 50  0000 C CNN
	1    6300 3000
	1    0    0    -1  
$EndComp
$Comp
L VPP #PWR03
U 1 1 58B2934E
P 6300 1300
F 0 "#PWR03" H 6300 1150 50  0001 C CNN
F 1 "VPP" H 6300 1450 50  0000 C CNN
F 2 "" H 6300 1300 50  0000 C CNN
F 3 "" H 6300 1300 50  0000 C CNN
	1    6300 1300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P302
U 1 1 58B29383
P 7300 2550
F 0 "P302" H 7300 2700 50  0000 C CNN
F 1 "POWER" V 7400 2550 50  0000 C CNN
F 2 "Local:2PIN_HEAD" H 7300 2550 50  0001 C CNN
F 3 "" H 7300 2550 50  0000 C CNN
	1    7300 2550
	1    0    0    1   
$EndComp
Wire Wire Line
	7100 2600 7000 2600
Wire Wire Line
	7000 2600 7000 2700
Wire Wire Line
	7100 2500 7000 2500
Wire Wire Line
	7000 2500 7000 2400
$Comp
L GND #PWR04
U 1 1 58B29410
P 7000 2700
F 0 "#PWR04" H 7000 2450 50  0001 C CNN
F 1 "GND" H 7000 2550 50  0000 C CNN
F 2 "" H 7000 2700 50  0000 C CNN
F 3 "" H 7000 2700 50  0000 C CNN
	1    7000 2700
	1    0    0    -1  
$EndComp
$Comp
L VPP #PWR05
U 1 1 58B29430
P 7000 2400
F 0 "#PWR05" H 7000 2250 50  0001 C CNN
F 1 "VPP" H 7000 2550 50  0000 C CNN
F 2 "" H 7000 2400 50  0000 C CNN
F 3 "" H 7000 2400 50  0000 C CNN
	1    7000 2400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P301
U 1 1 58B2951C
P 7300 1750
F 0 "P301" H 7300 1900 50  0000 C CNN
F 1 "SUPPLY" V 7400 1750 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x02_Pitch2.54mm" H 7300 1750 50  0001 C CNN
F 3 "" H 7300 1750 50  0000 C CNN
	1    7300 1750
	1    0    0    1   
$EndComp
Wire Wire Line
	7100 1800 7000 1800
Wire Wire Line
	7000 1800 7000 1900
Wire Wire Line
	7100 1700 7000 1700
Wire Wire Line
	7000 1700 7000 1600
$Comp
L GND #PWR06
U 1 1 58B29526
P 7000 1900
F 0 "#PWR06" H 7000 1650 50  0001 C CNN
F 1 "GND" H 7000 1750 50  0000 C CNN
F 2 "" H 7000 1900 50  0000 C CNN
F 3 "" H 7000 1900 50  0000 C CNN
	1    7000 1900
	1    0    0    -1  
$EndComp
$Comp
L +15V #PWR07
U 1 1 58B295AA
P 7000 1600
F 0 "#PWR07" H 7000 1450 50  0001 C CNN
F 1 "+15V" H 7000 1740 50  0000 C CNN
F 2 "" H 7000 1600 50  0000 C CNN
F 3 "" H 7000 1600 50  0000 C CNN
	1    7000 1600
	1    0    0    -1  
$EndComp
$Comp
L +15V #PWR08
U 1 1 58B29751
P 3700 1600
F 0 "#PWR08" H 3700 1450 50  0001 C CNN
F 1 "+15V" H 3700 1740 50  0000 C CNN
F 2 "" H 3700 1600 50  0000 C CNN
F 3 "" H 3700 1600 50  0000 C CNN
	1    3700 1600
	1    0    0    -1  
$EndComp
$Comp
L D_Schottky D301
U 1 1 58B297BE
P 2400 1800
F 0 "D301" H 2400 1900 50  0000 C CNN
F 1 "SS14" H 2400 1700 50  0000 C CNN
F 2 "Local:D_SMA_Standard" H 2400 1800 50  0001 C CNN
F 3 "" H 2400 1800 50  0000 C CNN
	1    2400 1800
	0    -1   -1   0   
$EndComp
$Comp
L +15V #PWR09
U 1 1 58B298C6
P 2400 1550
F 0 "#PWR09" H 2400 1400 50  0001 C CNN
F 1 "+15V" H 2400 1690 50  0000 C CNN
F 2 "" H 2400 1550 50  0000 C CNN
F 3 "" H 2400 1550 50  0000 C CNN
	1    2400 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 1550 2400 1650
$Comp
L C C301
U 1 1 58B29C07
P 2800 1850
F 0 "C301" H 2825 1950 50  0000 L CNN
F 1 "470n" H 2825 1750 50  0000 L CNN
F 2 "Local:C_1812" H 2838 1700 50  0001 C CNN
F 3 "" H 2800 1850 50  0000 C CNN
	1    2800 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1600 5900 1600
Wire Wire Line
	6000 2200 5900 2200
Wire Wire Line
	6000 3300 5900 3300
Wire Wire Line
	6000 3900 5900 3900
Text GLabel 5900 1600 0    47   Input ~ 0
G1H
Text GLabel 5900 2200 0    47   Input ~ 0
G1L
Text GLabel 5900 3300 0    47   Input ~ 0
G2H
Text GLabel 5900 3900 0    47   Input ~ 0
G2L
Text GLabel 5400 1800 2    47   Input ~ 0
G1H
Text GLabel 5400 2000 2    47   Input ~ 0
G1L
$Comp
L R R301
U 1 1 58B29FCD
P 5150 1800
F 0 "R301" V 5230 1800 50  0000 C CNN
F 1 "100" V 5150 1800 50  0000 C CNN
F 2 "Local:R_0603" V 5080 1800 50  0001 C CNN
F 3 "" H 5150 1800 50  0000 C CNN
	1    5150 1800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 1800 5400 1800
$Comp
L R R302
U 1 1 58B2A0B3
P 5150 2000
F 0 "R302" V 5230 2000 50  0000 C CNN
F 1 "100" V 5150 2000 50  0000 C CNN
F 2 "Local:R_0603" V 5080 2000 50  0001 C CNN
F 3 "" H 5150 2000 50  0000 C CNN
	1    5150 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5400 2000 5300 2000
Text GLabel 4800 1900 2    47   Input ~ 0
VS1
$Comp
L GND #PWR010
U 1 1 58B2A3CF
P 3700 2100
F 0 "#PWR010" H 3700 1850 50  0001 C CNN
F 1 "GND" H 3700 1950 50  0000 C CNN
F 2 "" H 3700 2100 50  0000 C CNN
F 3 "" H 3700 2100 50  0000 C CNN
	1    3700 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2000 2800 2100
Text GLabel 2800 2100 3    47   Input ~ 0
VS1
Text GLabel 4800 1700 2    47   Input ~ 0
VB1
$Comp
L C C302
U 1 1 58B2ABC9
P 3200 1850
F 0 "C302" H 3225 1950 50  0000 L CNN
F 1 "470n" H 3225 1750 50  0000 L CNN
F 2 "Local:C_1812" H 3238 1700 50  0001 C CNN
F 3 "" H 3200 1850 50  0000 C CNN
	1    3200 1850
	1    0    0    -1  
$EndComp
$Comp
L +15V #PWR011
U 1 1 58B2AC7D
P 3200 1600
F 0 "#PWR011" H 3200 1450 50  0001 C CNN
F 1 "+15V" H 3200 1740 50  0000 C CNN
F 2 "" H 3200 1600 50  0000 C CNN
F 3 "" H 3200 1600 50  0000 C CNN
	1    3200 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 58B2ACAF
P 3200 2100
F 0 "#PWR012" H 3200 1850 50  0001 C CNN
F 1 "GND" H 3200 1950 50  0000 C CNN
F 2 "" H 3200 2100 50  0000 C CNN
F 3 "" H 3200 2100 50  0000 C CNN
	1    3200 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2100 3200 2000
Wire Wire Line
	3200 1700 3200 1600
Text GLabel 2800 1600 1    47   Input ~ 0
VB1
Wire Wire Line
	2800 1600 2800 1700
Text GLabel 2400 2050 3    47   Input ~ 0
VB1
Wire Wire Line
	2400 1950 2400 2050
Wire Wire Line
	4800 1700 4700 1700
Text GLabel 3700 1800 0    47   Input ~ 0
IN1
Text GLabel 3700 1900 0    47   Input ~ 0
~SD1
$Comp
L IR2109 U302
U 1 1 58B2BC15
P 4250 3550
F 0 "U302" H 4250 3850 60  0000 C CNN
F 1 "IR2109" H 4250 3275 60  0000 C CNN
F 2 "Local:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 4250 3550 60  0001 C CNN
F 3 "" H 4250 3550 60  0000 C CNN
	1    4250 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3700 3700 3700
Wire Wire Line
	3700 3700 3700 3800
Wire Wire Line
	3800 3400 3700 3400
Wire Wire Line
	3700 3400 3700 3300
Wire Wire Line
	4700 3700 5000 3700
Wire Wire Line
	4700 3600 4800 3600
Wire Wire Line
	4700 3500 5000 3500
Wire Wire Line
	3800 3500 3700 3500
Wire Wire Line
	3800 3600 3700 3600
$Comp
L +15V #PWR013
U 1 1 58B2BC24
P 3700 3300
F 0 "#PWR013" H 3700 3150 50  0001 C CNN
F 1 "+15V" H 3700 3440 50  0000 C CNN
F 2 "" H 3700 3300 50  0000 C CNN
F 3 "" H 3700 3300 50  0000 C CNN
	1    3700 3300
	1    0    0    -1  
$EndComp
$Comp
L D_Schottky D302
U 1 1 58B2BC2A
P 2400 3500
F 0 "D302" H 2400 3600 50  0000 C CNN
F 1 "SS14" H 2400 3400 50  0000 C CNN
F 2 "Local:D_SMA_Standard" H 2400 3500 50  0001 C CNN
F 3 "" H 2400 3500 50  0000 C CNN
	1    2400 3500
	0    -1   -1   0   
$EndComp
$Comp
L +15V #PWR014
U 1 1 58B2BC30
P 2400 3250
F 0 "#PWR014" H 2400 3100 50  0001 C CNN
F 1 "+15V" H 2400 3390 50  0000 C CNN
F 2 "" H 2400 3250 50  0000 C CNN
F 3 "" H 2400 3250 50  0000 C CNN
	1    2400 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3250 2400 3350
$Comp
L C C303
U 1 1 58B2BC37
P 2800 3550
F 0 "C303" H 2825 3650 50  0000 L CNN
F 1 "470n" H 2825 3450 50  0000 L CNN
F 2 "Local:C_1812" H 2838 3400 50  0001 C CNN
F 3 "" H 2800 3550 50  0000 C CNN
	1    2800 3550
	1    0    0    -1  
$EndComp
Text GLabel 5400 3500 2    47   Input ~ 0
G2H
Text GLabel 5400 3700 2    47   Input ~ 0
G2L
$Comp
L R R303
U 1 1 58B2BC3F
P 5150 3500
F 0 "R303" V 5230 3500 50  0000 C CNN
F 1 "100" V 5150 3500 50  0000 C CNN
F 2 "Local:R_0603" V 5080 3500 50  0001 C CNN
F 3 "" H 5150 3500 50  0000 C CNN
	1    5150 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 3500 5400 3500
$Comp
L R R304
U 1 1 58B2BC46
P 5150 3700
F 0 "R304" V 5230 3700 50  0000 C CNN
F 1 "100" V 5150 3700 50  0000 C CNN
F 2 "Local:R_0603" V 5080 3700 50  0001 C CNN
F 3 "" H 5150 3700 50  0000 C CNN
	1    5150 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5400 3700 5300 3700
Text GLabel 4800 3600 2    47   Input ~ 0
VS2
$Comp
L GND #PWR015
U 1 1 58B2BC4E
P 3700 3800
F 0 "#PWR015" H 3700 3550 50  0001 C CNN
F 1 "GND" H 3700 3650 50  0000 C CNN
F 2 "" H 3700 3800 50  0000 C CNN
F 3 "" H 3700 3800 50  0000 C CNN
	1    3700 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3700 2800 3800
Text GLabel 2800 3800 3    47   Input ~ 0
VS2
Text GLabel 4800 3400 2    47   Input ~ 0
VB2
$Comp
L C C304
U 1 1 58B2BC57
P 3200 3550
F 0 "C304" H 3225 3650 50  0000 L CNN
F 1 "470n" H 3225 3450 50  0000 L CNN
F 2 "Local:C_1812" H 3238 3400 50  0001 C CNN
F 3 "" H 3200 3550 50  0000 C CNN
	1    3200 3550
	1    0    0    -1  
$EndComp
$Comp
L +15V #PWR016
U 1 1 58B2BC5D
P 3200 3300
F 0 "#PWR016" H 3200 3150 50  0001 C CNN
F 1 "+15V" H 3200 3440 50  0000 C CNN
F 2 "" H 3200 3300 50  0000 C CNN
F 3 "" H 3200 3300 50  0000 C CNN
	1    3200 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 58B2BC63
P 3200 3800
F 0 "#PWR017" H 3200 3550 50  0001 C CNN
F 1 "GND" H 3200 3650 50  0000 C CNN
F 2 "" H 3200 3800 50  0000 C CNN
F 3 "" H 3200 3800 50  0000 C CNN
	1    3200 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3800 3200 3700
Wire Wire Line
	3200 3400 3200 3300
Text GLabel 2800 3300 1    47   Input ~ 0
VB2
Wire Wire Line
	2800 3300 2800 3400
Text GLabel 2400 3750 3    47   Input ~ 0
VB2
Wire Wire Line
	2400 3650 2400 3750
Wire Wire Line
	4800 3400 4700 3400
Text GLabel 3700 3500 0    47   Input ~ 0
IN2
Text GLabel 3700 3600 0    47   Input ~ 0
~SD2
Wire Wire Line
	1500 2550 1600 2550
Wire Wire Line
	1500 2450 1600 2450
Text GLabel 1600 2550 2    47   Input ~ 0
IN1
Text GLabel 1600 2450 2    47   Input ~ 0
~SD1
Wire Wire Line
	1500 2850 1600 2850
Wire Wire Line
	1500 2650 1600 2650
Wire Wire Line
	1500 2350 1600 2350
Wire Wire Line
	1500 2750 1600 2750
$Comp
L GND #PWR018
U 1 1 58B2A828
P 1600 2350
F 0 "#PWR018" H 1600 2100 50  0001 C CNN
F 1 "GND" V 1600 2150 50  0000 C CNN
F 2 "" H 1600 2350 50  0000 C CNN
F 3 "" H 1600 2350 50  0000 C CNN
	1    1600 2350
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR019
U 1 1 58B2A82E
P 1600 2650
F 0 "#PWR019" H 1600 2400 50  0001 C CNN
F 1 "GND" V 1600 2450 50  0000 C CNN
F 2 "" H 1600 2650 50  0000 C CNN
F 3 "" H 1600 2650 50  0000 C CNN
	1    1600 2650
	0    -1   -1   0   
$EndComp
Text GLabel 1600 2850 2    47   Input ~ 0
IN2
Text GLabel 1600 2750 2    47   Input ~ 0
~SD2
$Comp
L PWR_FLAG #FLG020
U 1 1 58B2B197
P 4700 3000
F 0 "#FLG020" H 4700 3095 50  0001 C CNN
F 1 "PWR_FLAG" H 4700 3180 50  0000 C CNN
F 2 "" H 4700 3000 50  0000 C CNN
F 3 "" H 4700 3000 50  0000 C CNN
	1    4700 3000
	1    0    0    -1  
$EndComp
$Comp
L +15V #PWR021
U 1 1 58B2B2A5
P 5000 3000
F 0 "#PWR021" H 5000 2850 50  0001 C CNN
F 1 "+15V" H 5000 3140 50  0000 C CNN
F 2 "" H 5000 3000 50  0000 C CNN
F 3 "" H 5000 3000 50  0000 C CNN
	1    5000 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3000 4700 3100
Wire Wire Line
	4700 3100 5000 3100
Wire Wire Line
	5000 3100 5000 3000
$Comp
L VPP #PWR022
U 1 1 58B2B584
P 5700 3000
F 0 "#PWR022" H 5700 2850 50  0001 C CNN
F 1 "VPP" H 5700 3150 50  0000 C CNN
F 2 "" H 5700 3000 50  0000 C CNN
F 3 "" H 5700 3000 50  0000 C CNN
	1    5700 3000
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG023
U 1 1 58B2B5CE
P 5400 3000
F 0 "#FLG023" H 5400 3095 50  0001 C CNN
F 1 "PWR_FLAG" H 5400 3180 50  0000 C CNN
F 2 "" H 5400 3000 50  0000 C CNN
F 3 "" H 5400 3000 50  0000 C CNN
	1    5400 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3000 5400 3100
Wire Wire Line
	5400 3100 5700 3100
Wire Wire Line
	5700 3100 5700 3000
Text GLabel 5400 2600 2    47   Input ~ 0
VS1
$Comp
L PWR_FLAG #FLG024
U 1 1 58B2B9E8
P 5300 2500
F 0 "#FLG024" H 5300 2595 50  0001 C CNN
F 1 "PWR_FLAG" H 5300 2680 50  0000 C CNN
F 2 "" H 5300 2500 50  0000 C CNN
F 3 "" H 5300 2500 50  0000 C CNN
	1    5300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2600 5300 2600
Wire Wire Line
	5300 2600 5300 2500
Text GLabel 5400 4300 2    47   Input ~ 0
VS2
$Comp
L PWR_FLAG #FLG025
U 1 1 58B2BDF1
P 5300 4200
F 0 "#FLG025" H 5300 4295 50  0001 C CNN
F 1 "PWR_FLAG" H 5300 4380 50  0000 C CNN
F 2 "" H 5300 4200 50  0000 C CNN
F 3 "" H 5300 4200 50  0000 C CNN
	1    5300 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4200 5300 4300
Wire Wire Line
	5300 4300 5400 4300
Text GLabel 4900 2600 2    47   Input ~ 0
VB1
$Comp
L PWR_FLAG #FLG026
U 1 1 58B2BFF1
P 4800 2500
F 0 "#FLG026" H 4800 2595 50  0001 C CNN
F 1 "PWR_FLAG" H 4800 2680 50  0000 C CNN
F 2 "" H 4800 2500 50  0000 C CNN
F 3 "" H 4800 2500 50  0000 C CNN
	1    4800 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2600 4800 2600
Wire Wire Line
	4800 2600 4800 2500
Text GLabel 4900 4300 2    47   Input ~ 0
VB2
$Comp
L PWR_FLAG #FLG027
U 1 1 58B2C024
P 4800 4200
F 0 "#FLG027" H 4800 4295 50  0001 C CNN
F 1 "PWR_FLAG" H 4800 4380 50  0000 C CNN
F 2 "" H 4800 4200 50  0000 C CNN
F 3 "" H 4800 4200 50  0000 C CNN
	1    4800 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4200 4800 4300
Wire Wire Line
	4800 4300 4900 4300
$Comp
L GND #PWR028
U 1 1 58B2C3D2
P 4300 2600
F 0 "#PWR028" H 4300 2350 50  0001 C CNN
F 1 "GND" H 4300 2450 50  0000 C CNN
F 2 "" H 4300 2600 50  0000 C CNN
F 3 "" H 4300 2600 50  0000 C CNN
	1    4300 2600
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG029
U 1 1 58B2C485
P 4300 2500
F 0 "#FLG029" H 4300 2595 50  0001 C CNN
F 1 "PWR_FLAG" H 4300 2680 50  0000 C CNN
F 2 "" H 4300 2500 50  0000 C CNN
F 3 "" H 4300 2500 50  0000 C CNN
	1    4300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2500 4300 2600
$Comp
L R R310
U 1 1 58B2D6AA
P 2750 2500
F 0 "R310" V 2830 2500 50  0000 C CNN
F 1 "R01" V 2750 2500 50  0000 C CNN
F 2 "Local:R_2512" V 2680 2500 50  0001 C CNN
F 3 "" H 2750 2500 50  0000 C CNN
	1    2750 2500
	0    -1   -1   0   
$EndComp
$Comp
L R R312
U 1 1 58B2D913
P 3250 2500
F 0 "R312" V 3330 2500 50  0000 C CNN
F 1 "R01" V 3250 2500 50  0000 C CNN
F 2 "Local:R_2512" V 3180 2500 50  0001 C CNN
F 3 "" H 3250 2500 50  0000 C CNN
	1    3250 2500
	0    -1   -1   0   
$EndComp
$Comp
L R R311
U 1 1 58B2D978
P 2750 2700
F 0 "R311" V 2830 2700 50  0000 C CNN
F 1 "R01" V 2750 2700 50  0000 C CNN
F 2 "Local:R_2512" V 2680 2700 50  0001 C CNN
F 3 "" H 2750 2700 50  0000 C CNN
	1    2750 2700
	0    -1   -1   0   
$EndComp
$Comp
L R R313
U 1 1 58B2D9EC
P 3250 2700
F 0 "R313" V 3330 2700 50  0000 C CNN
F 1 "R01" V 3250 2700 50  0000 C CNN
F 2 "Local:R_2512" V 3180 2700 50  0001 C CNN
F 3 "" H 3250 2700 50  0000 C CNN
	1    3250 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2900 2500 3100 2500
Wire Wire Line
	3000 2500 3000 2700
Wire Wire Line
	2900 2700 3100 2700
Connection ~ 3000 2500
Connection ~ 3000 2700
Wire Wire Line
	2500 2500 2600 2500
Wire Wire Line
	2500 2500 2500 2800
Wire Wire Line
	2500 2700 2600 2700
Wire Wire Line
	3400 2500 3600 2500
Wire Wire Line
	3500 2500 3500 2800
Wire Wire Line
	3500 2700 3400 2700
Connection ~ 2500 2700
$Comp
L GNDREF #PWR030
U 1 1 58B2E001
P 6300 2500
F 0 "#PWR030" H 6300 2250 50  0001 C CNN
F 1 "GNDREF" H 6300 2350 50  0000 C CNN
F 2 "" H 6300 2500 50  0000 C CNN
F 3 "" H 6300 2500 50  0000 C CNN
	1    6300 2500
	1    0    0    -1  
$EndComp
$Comp
L GNDREF #PWR031
U 1 1 58B2E0DB
P 6300 4200
F 0 "#PWR031" H 6300 3950 50  0001 C CNN
F 1 "GNDREF" H 6300 4050 50  0000 C CNN
F 2 "" H 6300 4200 50  0000 C CNN
F 3 "" H 6300 4200 50  0000 C CNN
	1    6300 4200
	1    0    0    -1  
$EndComp
$Comp
L GNDREF #PWR032
U 1 1 58B2E1C7
P 3500 2800
F 0 "#PWR032" H 3500 2550 50  0001 C CNN
F 1 "GNDREF" H 3500 2650 50  0000 C CNN
F 2 "" H 3500 2800 50  0000 C CNN
F 3 "" H 3500 2800 50  0000 C CNN
	1    3500 2800
	1    0    0    -1  
$EndComp
Connection ~ 3500 2700
$Comp
L CONN_01X08 P304
U 1 1 58B2E95D
P 1300 2700
F 0 "P304" H 1300 3150 50  0000 C CNN
F 1 "CONTROL" V 1400 2700 50  0000 C CNN
F 2 "Local:Pin_Header_Straight_1x08_Pitch2.00mm" H 1300 2700 50  0001 C CNN
F 3 "" H 1300 2700 50  0000 C CNN
	1    1300 2700
	-1   0    0    1   
$EndComp
Connection ~ 3500 2500
Text GLabel 3600 2500 2    47   Input ~ 0
IS2
Text GLabel 1600 2950 2    47   Input ~ 0
IS2
Wire Wire Line
	1600 2950 1500 2950
Wire Wire Line
	1500 3050 1600 3050
$Comp
L GND #PWR033
U 1 1 58B2CCC6
P 1600 3050
F 0 "#PWR033" H 1600 2800 50  0001 C CNN
F 1 "GND" V 1600 2850 50  0000 C CNN
F 2 "" H 1600 3050 50  0000 C CNN
F 3 "" H 1600 3050 50  0000 C CNN
	1    1600 3050
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
